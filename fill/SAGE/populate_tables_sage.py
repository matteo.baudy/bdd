from sqlalchemy import create_engine, MetaData, select
from faker import Faker
import sys
import random
import datetime
import configparser
from connect import engine

engine = engine
metadata = MetaData()
metadata.reflect(bind=engine)

# Instantiate faker object
faker = Faker()


reunion = metadata.tables["Reunion"]
demande = metadata.tables["Demande"]
rendez_vous= metadata.tables["Rendez_Vous"]
client = metadata.tables["Client"]
agent = metadata.tables["Agent"]
appartient = metadata.tables["Appartient"]
supervisee = metadata.tables["Supervisee"]
present = metadata.tables["Present"]


database=[]

try :
    database.append((reunion,10000))
    database.append((demande,20000))
    database.append((rendez_vous,10000))
    database.append((client,15000))
    database.append((agent,15000))
    database.append((appartient,15000))
    database.append((supervisee,15000))
    database.append((present,15000))
except KeyError as err:
    print("error : Metadata.tables "+str(err)+" not found")

# product list
product_list = ["hat", "cap", "shirt", "sweater", "sweatshirt", "shorts",
    "jeans", "sneakers", "boots", "coat", "accessories"]


class GenerateData:
    """
    generate a specific number of records to a target table
    """

    def __init__(self,table):
        """
        initialize command line arguments
        """
        self.table = table[0]
        self.num_records = table[1]


    def create_data(self):
        """
        using faker library, generate data and execute DML
        """
        
        if self.table.name == "Reunion":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    date_obj = datetime.datetime.now() - datetime.timedelta(days=random.randint(0,30))
                    insert_stmt = self.table.insert().values(
                        Type_de_reunion = faker.random_int(1,10),
                        Date_Reunion = date_obj.strftime("%Y/%m/%d"),
                        Duree_Reunion = faker.time()
                    )
                    conn.execute(insert_stmt)
        

        if self.table.name == "Demande":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                        Motif_Demande = faker.word(),
                        Type_Aide = faker.word(),
                        Type_Partenaire = faker.word(),
                        Montant = faker.random_int(100,100000)
                    )
                    conn.execute(insert_stmt)
       

        if self.table.name == "Rendez_Vous":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                        Date_RDV = faker.date(),
                        Motif_Absence = faker.word(),
                        Duree_RDV = faker.time(),
                        Id_Demande = random.choice(conn.execute(select([demande.c.Id_Demande])).fetchall())[0]

                    )
                    conn.execute(insert_stmt)
        

        if self.table.name == "Client":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                        Nom_Client=faker.last_name(),
                        Adresse_Postale_Client=faker.address(),
                        Composition_Familiale_Client=faker.paragraph(nb_sentences=1),
                        Numero_Telephone="06"+str(faker.random_int(11111111,99999999)),
                        Prenom_Client=faker.first_name(),
                        Adresse_Mail_Client=faker.email(),
                        Position_dossier_client=faker.random_int(0,3000)
                    )
                    conn.execute(insert_stmt)
        
        if self.table.name == "Agent":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                        Nom_Agent=faker.last_name(),
                        Prenom_Agent=faker.first_name(),
                        Regime_Activite_Agent=faker.random_int(0,10)
                    )
                    conn.execute(insert_stmt)
        

        if self.table.name == "Appartient":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    try:
                        insert_stmt = self.table.insert().values(
                            Id_Client = random.choice(conn.execute(select([client.c.Id_Client])).fetchall())[0],
                            Id_Demande = random.choice(conn.execute(select([demande.c.Id_Demande])).fetchall())[0]
                        )
                        conn.execute(insert_stmt)
                    except:
                        print("A couple \"Appartient\" already exists")

        if self.table.name == "Supervisee":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    try:
                        insert_stmt = self.table.insert().values(
                            Id_Agent = random.choice(conn.execute(select([agent.c.Id_Agent])).fetchall())[0],
                            Id_Demande = random.choice(conn.execute(select([demande.c.Id_Demande])).fetchall())[0]
                        )
                        conn.execute(insert_stmt)
                    except:
                        print("A couple \"Supervisee\" already exists")

        if self.table.name == "Present":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    try:
                        insert_stmt = self.table.insert().values(
                            Id_Agent = random.choice(conn.execute(select([agent.c.Id_Agent])).fetchall())[0],
                            Id_Reunion = random.choice(conn.execute(select([reunion.c.Id_Reunion])).fetchall())[0]
                        )
                        conn.execute(insert_stmt)
                    except:
                        print("A couple \"Present\" already exists")

if __name__ == "__main__":
    for i in database:
        print(i)
        generate_data = GenerateData(i)
        generate_data.create_data()
