from sqlalchemy import create_engine, MetaData, select
from faker import Faker
import sys
import random
import datetime
import configparser
from connect import engine

engine = engine
metadata = MetaData()
metadata.reflect(bind=engine)

# Instantiate faker object
faker = Faker()


Region = metadata.tables["Region"]
Vaccin = metadata.tables["Vaccin"]
Protocole = metadata.tables["Protocole"]
Vaccination = metadata.tables["Vaccination"]
Infection = metadata.tables["Infection"]
Variant = metadata.tables["Variant"]
Type_traitement = metadata.tables["Type_traitement"]
Traitement = metadata.tables["Traitement"]
Etablissement = metadata.tables["Etablissement"]
Personne = metadata.tables["Personne"]

database=[]

try :
    database.append((Region,13))
    database.append((Vaccin,4))
    database.append((Personne,70000))
    database.append((Protocole,1000))
    database.append((Vaccination,35000))
    database.append((Variant,10))
    database.append((Infection,10000))
    database.append((Etablissement,1000))
    database.append((Type_traitement,4))
    database.append((Traitement,35000))
except KeyError as err:
    print("error : Metadata.tables "+str(err)+" not found")

# sexe, vaccin's type, and fabriquant list
sexe = ["HOMME","FEMME"]
type_vaccin = ['ATTENUE', 'INACTIVE']
fabricant = ['ModeRNA', 'Pfizer', 'Johnson', 'BioNTech', 'Novavax']
region = ["Bretagne", "Ile de France", "Normandie", "Poitou Charente"]
vaccin = ["covid", "tetanos", "pif"]
etablissement = ["hopital", "clinique", "medecin generaliste"]

class GenerateData:
    """
    generate a specific number of records to a target table
    """

    def __init__(self,table):
        """
        initialize command line arguments
        """
        self.table = table[0]
        self.num_records = table[1]


    def create_data(self):
        """
        using faker library, generate data and execute DML
        """

        if self.table.name == "Region":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                        nom = random.choice(region)
                    )
                    conn.execute(insert_stmt)

        if self.table.name == "Vaccin":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                        nom = random.choice(vaccin),
                        fabriquant = random.choice(fabricant),
                        type_vaccin = random.choice(type_vaccin)
                    )
                    conn.execute(insert_stmt)

        if self.table.name == "Protocole":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                        nom = faker.text()[:5]
                    )
                    conn.execute(insert_stmt)

        if self.table.name == "Personne":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                        sexe = random.choice(sexe),
                        age = random.randint(0,100),
                        id_region = random.choice(conn.execute(select([Region.c.id_region])).fetchall())[0]
                    )
                    conn.execute(insert_stmt)

        if self.table.name == "Vaccination":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                        id_personne = random.choice(conn.execute(select([Personne.c.id_personne])).fetchall())[0],
                        id_vaccin = random.choice(conn.execute(select([Vaccin.c.id_vaccin])).fetchall())[0]
                    )
                    conn.execute(insert_stmt)

        if self.table.name == "Variant":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                        nom = random.choice(vaccin)
                    )
                    conn.execute(insert_stmt)

        if self.table.name == "Infection":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                        id_variant = random.choice(conn.execute(select([Variant.c.id_variant])).fetchall())[0],
                        id_personne = random.choice(conn.execute(select([Personne.c.id_personne])).fetchall())[0]
                    )
                    conn.execute(insert_stmt)

        if self.table.name == "Etablissement":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                        nom = random.choice(etablissement)
                    )
                    conn.execute(insert_stmt)

        if self.table.name == "Type_traitement":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                        descriptif = faker.text()[:5]
                    )
                    conn.execute(insert_stmt)

        if self.table.name == "Traitement":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                        idInfection = random.choice(conn.execute(select([Infection.c.id_infection])).fetchall())[0],
                        idType_traitement = random.choice(conn.execute(select([Type_traitement.c.id_type_traitement])).fetchall())[0],
                        idEtablissement_sante = random.choice(conn.execute(select([Etablissement.c.id_etablissement])).fetchall())[0]
                    )
                    conn.execute(insert_stmt)

if __name__ == "__main__":
    for i in database:
        generate_data = GenerateData(i)
        generate_data.create_data()
