from sqlalchemy import create_engine, MetaData, select
from faker import Faker
import sys
import random
import datetime
import configparser
from connect import engine

engine = engine
metadata = MetaData()
metadata.reflect(bind=engine)

# Instantiate faker object
faker = Faker()


Switch_virtuel = metadata.tables["Switch_Virtuel"]
Serveur = metadata.tables["Serveur"]
Pool_infra = metadata.tables["PoolInfra"]
Machine = metadata.tables["Machine"]
Groupe = metadata.tables["Groupe"]
Utilisateur = metadata.tables["Utilisateur"]

database=[]

try :
    database.append((Switch_virtuel,1000))
    database.append((Serveur,200*60))
    database.append((Pool_infra,60*200))
    database.append((Machine,2000))
    database.append((Groupe,1000))
    database.append((Utilisateur,1000))
except KeyError as err:
    print("error : Metadata.tables "+str(err)+" not found")

# type os and state list
type_os = ["LINUX", "WINDOWS", "MACOS"]
etat = ["ON", "OFF"]


class GenerateData:
    """
    generate a specific number of records to a target table
    """

    def __init__(self,table):
        """
        initialize command line arguments
        """
        self.table = table[0]
        self.num_records = table[1]


    def create_data(self):
        """
        using faker library, generate data and execute DML
        """

        if self.table.name == "Switch_Virtuel":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                        compteur = random.randint(0,10),
                        vmd = random.randint(0,100)
                    )
                    conn.execute(insert_stmt)

        if self.table.name == "Serveur":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                    )
                    conn.execute(insert_stmt)
        if self.table.name == "PoolInfra":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                        serveur = random.choice(conn.execute(select([Serveur.c.id_serveur])).fetchall())[0]
                    )
                    conn.execute(insert_stmt)

        if self.table.name == "Machine":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                        nom = faker.text()[:10],
                        type_os = random.choice(type_os),
                        taille_ram = random.randint(0,128),
                        nombre_coeur = random.randint(0,64),
                        taille_disque_dur = random.randint(0,1000),
                        etat = random.choice(etat),
                        switch = random.choice(conn.execute(select([Switch_virtuel.c.id_switch])).fetchall())[0],
                        idPool = random.choice(conn.execute(select([Pool_infra.c.id_pool])).fetchall())[0]
                    )
                    conn.execute(insert_stmt)

        if self.table.name == "Groupe":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                        nom = faker.text()[:10],
                        idPool = random.choice(conn.execute(select([Pool_infra.c.id_pool])).fetchall())[0]
                    )
                    conn.execute(insert_stmt)

        if self.table.name == "Utilisateur":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                        nom = faker.text()[:10],
                        pseudo = faker.text()[:10],
                        mot_de_passe = faker.text()[:10],
                        idGroupe = random.choice(conn.execute(select([Groupe.c.id_groupe])).fetchall())[0]
                    )
                    conn.execute(insert_stmt)

if __name__ == "__main__":
    for i in database:
        generate_data = GenerateData(i)
        generate_data.create_data()
