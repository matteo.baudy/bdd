from sqlalchemy import create_engine, MetaData, select
from faker import Faker
import sys
import random
import datetime
import configparser
from connect import engine

engine = engine
metadata = MetaData()
metadata.reflect(bind=engine)

# Instantiate faker object
faker = Faker()


Tombola = metadata.tables["Tombola"]
Ticket = metadata.tables["Ticket"]
Adherent = metadata.tables["Adherent"]
Possede = metadata.tables["Possede"]

database=[]

try :
    database.append((Tombola,100*50))
    database.append((Ticket,2000*50))
    database.append((Adherent,2000))
    database.append((Possede,2000*50))
except KeyError as err:
    print("error : Metadata.tables "+str(err)+" not found")

# product list
product_list = ["hat", "cap", "shirt", "sweater", "sweatshirt", "shorts",
    "jeans", "sneakers", "boots", "coat", "accessories"]


class GenerateData:
    """
    generate a specific number of records to a target table
    """

    def __init__(self,table):
        """
        initialize command line arguments
        """
        self.table = table[0]
        self.num_records = table[1]


    def create_data(self):
        """
        using faker library, generate data and execute DML
        """

        if self.table.name == "Tombola":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                        prix = random.randint(0,9999999),
                        numero_gagnant = random.randint(0,200)
                    )
                    conn.execute(insert_stmt)

        if self.table.name == "Ticket":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                        ticket_numero = random.randint(0,200),
                        id_ticket_tombola = random.choice(conn.execute(select([Tombola.c.id_tombola])).fetchall())[0]
                    )
                    conn.execute(insert_stmt)

        if self.table.name == "Adherent":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                        nom = faker.text()[:10],
                        prenom = faker.text()[:10],
                        somme_gagnee = random.randint(0,99999999),
                        idTicket = random.choice(conn.execute(select([Ticket.c.id_ticket])).fetchall())[0]
                    )
                    conn.execute(insert_stmt)

        if self.table.name == "Possede":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                        numero = random.choice(conn.execute(select([Ticket.c.ticket_numero])).fetchall())[0],
                        idTombola = random.choice(conn.execute(select([Tombola.c.id_tombola])).fetchall())[0]
                    )
                    conn.execute(insert_stmt)

if __name__ == "__main__":
    for i in database:
        generate_data = GenerateData(i)
        generate_data.create_data()
