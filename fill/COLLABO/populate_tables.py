from sqlalchemy import create_engine, MetaData, select
from faker import Faker
import sys
import random
import datetime
import configparser
from connect import engine

engine = engine
metadata = MetaData()
metadata.reflect(bind=engine)

# Instantiate faker object
faker = Faker()


analyse = metadata.tables["Analyse"]
echantillon = metadata.tables["Echantillon"]
medecin = metadata.tables["Medecin"]
ordonance = metadata.tables["Ordonance"]
patient = metadata.tables["Patient"]

database=[]
nb_lignes=3120000

try :
    #database.append((analyse,nb_lignes))
    database.append((echantillon,nb_lignes))
    database.append((medecin,nb_lignes))
    database.append((ordonance,nb_lignes))
    #database.append((patient,nb_lignes))
except KeyError as err:
    print("error : Metadata.tables "+str(err)+" not found")

# product list
product_list = ["hat", "cap", "shirt", "sweater", "sweatshirt", "shorts",
    "jeans", "sneakers", "boots", "coat", "accessories"]


class GenerateData:
    """
    generate a specific number of records to a target table
    """

    def __init__(self,table):
        """
        initialize command line arguments
        """
        self.table = table[0]
        self.num_records = table[1]


    def create_data(self):
        """
        using faker library, generate data and execute DML
        """

        if self.table.name == "Analyse":
            k=0
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                        Id_Analyse = k,
                        Type_Analyse = faker.word(),
                        Url_du_resultat_PDF = "http://ensibf.lol/example.pdf",
                        Id_Ordonance = random.choice(conn.execute(select([ordonance.c.Id_Ordonance])).fetchall())[0],
                        Siret = random.choice(conn.execute(select([medecin.c.Siret])).fetchall())[0],
                        Id_Echantillon = random.choice(conn.execute(select([echantillon.c.Id_Echantillon])).fetchall())[0]
                    )
                    conn.execute(insert_stmt)
                    k+=1

        if self.table.name == "Echantillon":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                        Type_de_prelevement = faker.word()
                    )
                    conn.execute(insert_stmt)

        if self.table.name == "Medecin":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                        Adresse_Medecin = "5 Rue du JPP DE LA BDD",
                        Nom_Medecin = faker.name(),
                        Email = faker.email()
                    )
                    conn.execute(insert_stmt)

        if self.table.name == "Ordonance":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                        Papier_ou_non=random.choice([0,1]),
                        Date_Ordonance=faker.date(),
                        Url_de_l_ordonance=faker.word()
                    )
                    conn.execute(insert_stmt)

        if self.table.name == "Patient":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                        Type_Couverture_Social=faker.word(),
                        Prenom=faker.first_name(),
                        Nom=faker.last_name(),
                        Date_Naissance=str(faker.date()),
                        Poids=str(random.randint(50,200))+"kg",
                        Taille=str(random.uniform(1.40,2.10))[:4]+"m",
                        Siret=random.choice(conn.execute(select([medecin.c.Siret])).fetchall())[0],
                        Id_Analyse=random.choice(conn.execute(select([analyse.c.Id_Analyse])).fetchall())[0]
                    )
                    conn.execute(insert_stmt)

if __name__ == "__main__":
    for i in database:
        generate_data = GenerateData(i)
        generate_data.create_data()
