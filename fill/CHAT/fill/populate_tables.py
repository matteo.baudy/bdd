from sqlalchemy import create_engine, MetaData, select
from faker import Faker
import sys
import random
import datetime
import configparser
from connect import engine

engine = engine
metadata = MetaData()
metadata.reflect(bind=engine)

# Instantiate faker object
faker = Faker()


Chats = metadata.tables["Chat"]
Organismes = metadata.tables["Organisme"]
Veterinaires = metadata.tables["Veterinaire"]
Historique = metadata.tables["Historique"]

database=[]

try :
    database.append((Chats,11000000))
    database.append((Organismes,10000))
    database.append((Veterinaires,10000))
    database.append((Historique,100000))
except KeyError as err:
    print("error : Metadata.tables "+str(err)+" not found")

# product list
races = ["Maine Coon", "Egyptien", "Europeen", "Abyssin", "Siamois", "Sphinx"]
sexe = [0,1,2]
etat = ['PLACE', 'ERRANCE', 'ATTENTE']

class GenerateData:
    """
    generate a specific number of records to a target table
    """

    def __init__(self,table):
        """
        initialize command line arguments
        """
        self.table = table[0]
        self.num_records = table[1]


    def create_data(self):
        """
        using faker library, generate data and execute DML
        """

        if self.table.name == "Chat":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    my_date_reperage = faker.date()
                    insert_stmt = self.table.insert().values(
                        sexe = random.choice(sexe),
                        departement_decouverte = 56,
                        id_chat = str(random.randint(100000000000000000000000000000,1000000000000000000000000000000)), #29 chiffres pour l'identification du chat
                        race = random.choice(races),
                        date_reperage = my_date_reperage,
                        date_prise_charge_refuge = my_date_reperage,
                        etat = random.choice(etat),
                        chemin_de_vie = faker.text()[:10],
                        localisation = faker.address()[:10]
                    )
                    conn.execute(insert_stmt)

        if self.table.name == "Organisme":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    my_date_inscription = faker.date()
                    insert_stmt = self.table.insert().values(
                        localisation = faker.address()[:10],
                        nom = faker.text()[:10],
                        date_inscription = my_date_inscription,
                        date_creation = my_date_inscription
                    )
                    conn.execute(insert_stmt)

        if self.table.name == "Veterinaire":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                        idOrga = random.choice(conn.execute(select([Organismes.c.siret])).fetchall())[0], #29 chiffres pour l'identification du veterinaire
                        nom = faker.text()[:10],
                        localisation = faker.address()[:10],
                        nb_traite = random.randint(4,87)
                    )
                    conn.execute(insert_stmt)

        if self.table.name == "Historique":
            with engine.begin() as conn:
                for _ in range(self.num_records):
                    insert_stmt = self.table.insert().values(
                        idChat = random.choice(conn.execute(select([Chats.c.id_chat])).fetchall())[0],
                        idOrga = random.choice(conn.execute(select([Organismes.c.siret])).fetchall())[0]
                    )
                    conn.execute(insert_stmt)

if __name__ == "__main__":
    for i in database:
        generate_data = GenerateData(i)
        generate_data.create_data()
