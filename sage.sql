#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: Reunion
#------------------------------------------------------------

CREATE TABLE Reunion(
        Id_Reunion      Int  Auto_increment  NOT NULL ,
        Type_de_reunion Int NOT NULL ,
        Date_Reunion    Date NOT NULL ,
        Duree_Reunion   Time NOT NULL
	,CONSTRAINT Reunion_PK PRIMARY KEY (Id_Reunion)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Demande
#------------------------------------------------------------

CREATE TABLE Demande(
        Id_Demande      Int  Auto_increment  NOT NULL ,
        Motif_Demande   Varchar (50) NOT NULL ,
        Type_Aide       Varchar (50) NOT NULL ,
        Type_Partenaire Varchar (50) NOT NULL ,
        Montant         Int NOT NULL
	,CONSTRAINT Demande_PK PRIMARY KEY (Id_Demande)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Rendez-Vous
#------------------------------------------------------------

CREATE TABLE Rendez_Vous(
        Id_RDV        Int  Auto_increment  NOT NULL ,
        Date_RDV      Date NOT NULL ,
        Motif_Absence Varchar (50) NOT NULL ,
        Duree_RDV     Time NOT NULL ,
        Id_Demande    Int NOT NULL
	,CONSTRAINT Rendez_Vous_PK PRIMARY KEY (Id_RDV)

	,CONSTRAINT Rendez_Vous_Demande_FK FOREIGN KEY (Id_Demande) REFERENCES Demande(Id_Demande)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Client
#------------------------------------------------------------

CREATE TABLE Client(
        Id_Client                    Int  Auto_increment  NOT NULL ,
        Nom_Client                   Varchar (255) NOT NULL ,
        Adresse_Postale_Client       Varchar (255) NOT NULL ,
        Composition_Familiale_Client Varchar (255) NOT NULL ,
        Numero_Telephone             Varchar (10) NOT NULL ,
        Prenom_Client                Varchar (255) NOT NULL ,
        Adresse_Mail_Client          Varchar (255) NOT NULL ,
        Position_dossier_client      Int NOT NULL
	,CONSTRAINT Client_PK PRIMARY KEY (Id_Client)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Agent
#------------------------------------------------------------

CREATE TABLE Agent(
        Id_Agent              Int  Auto_increment  NOT NULL ,
        Nom_Agent             Varchar (255) NOT NULL ,
        Prenom_Agent          Varchar (255) NOT NULL ,
        Regime_Activite_Agent Int NOT NULL
	,CONSTRAINT Agent_PK PRIMARY KEY (Id_Agent)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Appartient
#------------------------------------------------------------

CREATE TABLE Appartient(
        Id_Client  Int NOT NULL ,
        Id_Demande Int NOT NULL
	,CONSTRAINT Appartient_PK PRIMARY KEY (Id_Client,Id_Demande)

	,CONSTRAINT Appartient_Client_FK FOREIGN KEY (Id_Client) REFERENCES Client(Id_Client)
	,CONSTRAINT Appartient_Demande0_FK FOREIGN KEY (Id_Demande) REFERENCES Demande(Id_Demande)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Supervis�e
#------------------------------------------------------------

CREATE TABLE Supervisee(
        Id_Agent   Int NOT NULL ,
        Id_Demande Int NOT NULL
	,CONSTRAINT Supervisee_PK PRIMARY KEY (Id_Agent,Id_Demande)

	,CONSTRAINT Supervisee_Agent_FK FOREIGN KEY (Id_Agent) REFERENCES Agent(Id_Agent)
	,CONSTRAINT Supervisee_Demande0_FK FOREIGN KEY (Id_Demande) REFERENCES Demande(Id_Demande)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Pr�sent
#------------------------------------------------------------

CREATE TABLE Present(
        Id_Agent   Int NOT NULL ,
        Id_Reunion Int NOT NULL
	,CONSTRAINT Present_PK PRIMARY KEY (Id_Agent,Id_Reunion)

	,CONSTRAINT Present_Agent_FK FOREIGN KEY (Id_Agent) REFERENCES Agent(Id_Agent)
	,CONSTRAINT Present_Reunion0_FK FOREIGN KEY (Id_Reunion) REFERENCES Reunion(Id_Reunion)
)ENGINE=InnoDB;

