DROP TABLE IF EXISTS Chat;
DROP TABLE IF EXISTS Organisme;
DROP TABLE IF EXISTS Veterinaire;
DROP TABLE IF EXISTS Historique;

CREATE OR REPLACE TABLE Chat(
    numero  INT NOT NULL AUTO_INCREMENT,
    sexe    INT,
    departement_decouverte  INT,
    id_chat VARCHAR(30) NOT NULL UNIQUE,
    race    VARCHAR(20),
    date_reperage DATE DEFAULT CURRENT_DATE,
    date_prise_charge_refuge DATE DEFAULT CURRENT_DATE,
    etat    ENUM('ERRANCE', 'ATTENTE', 'PLACE'),
    chemin_de_vie VARCHAR(20),
    localisation    VARCHAR(20),
    PRIMARY KEY (numero),
    CONSTRAINT ck_sexe CHECK (sexe IN (0,1,2))
);

CREATE OR REPLACE TABLE Organisme(
    siret   INT NOT NULL AUTO_INCREMENT,
    localisation    VARCHAR(40),
    nom VARCHAR(20) NOT NULL,
    PRIMARY KEY (siret),
    date_inscription DATE NOT NULL DEFAULT CURRENT_DATE,
    date_creation   DATE DEFAULT CURRENT_DATE
);

CREATE OR REPLACE TABLE Veterinaire(
    siret INT NOT NULL AUTO_INCREMENT,
    idOrga INT NOT NULL,
    nom VARCHAR(20),
    localisation VARCHAR(20),
    nb_traite INT,
    PRIMARY KEY (siret),
    CONSTRAINT  ck_nb_traite CHECK (nb_traite >= 0),
    CONSTRAINT fk_veterinaire_organisme FOREIGN KEY (idOrga) REFERENCES Organisme(siret)
);

CREATE OR REPLACE TABLE Historique(
    idChat VARCHAR(30) NOT NULL,
    idOrga INT NOT NULL,
    date_histo    DATE DEFAULT CURRENT_DATE,
    duree   TIME DEFAULT CURRENT_TIME,
    CONSTRAINT fk_chat_hisorique FOREIGN KEY (idChat) REFERENCES Chat(id_chat),
    CONSTRAINT fk_organisme_hisorique FOREIGN KEY (idOrga) REFERENCES Organisme(siret)
);