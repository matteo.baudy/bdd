DROP TABLE IF EXISTS Switch_Virtuel;
DROP TABLE IF EXISTS Serveur;
DROP TABLE IF EXISTS Machine;
DROP TABLE IF EXISTS PoolInfra;
DROP TABLE IF EXISTS Groupe;
DROP TABLE IF EXISTS Utilisateur;

CREATE TABLE IF NOT EXISTS Switch_Virtuel (
    id_switch INT NOT NULL AUTO_INCREMENT,
    compteur INT,
    vmd INT,
    PRIMARY KEY (id_switch)
);

CREATE TABLE IF NOT EXISTS Serveur(
    id_serveur INT NOT NULL AUTO_INCREMENT,
    PRIMARY KEY (id_serveur)
);

CREATE TABLE IF NOT EXISTS PoolInfra (
    id_pool INT NOT NULL AUTO_INCREMENT,
    serveur INT,
    PRIMARY KEY (id_pool),
    CONSTRAINT fk_poolInfra_serveur FOREIGN KEY (serveur) REFERENCES Serveur(id_serveur)
);

CREATE TABLE IF NOT EXISTS Machine (
    id_machine INT NOT NULL AUTO_INCREMENT,
    nom VARCHAR(15) NOT NULL,
    type_os ENUM('LINUX', 'WINDOWS', 'MACOS'),
    taille_ram INT CHECK (taille_ram <= 128),
    nombre_coeur INT CHECK (nombre_coeur <= 64),
    taille_disque_dur INT CHECK (taille_disque_dur < 100000),
    etat ENUM('ON', 'OFF'),
    switch INT,
    idPool INT,
    PRIMARY KEY (id_machine),
    CONSTRAINT fk_groupe_poolInfra2 FOREIGN KEY (idPool) REFERENCES PoolInfra(id_pool),
    CONSTRAINT fk_machine_switch FOREIGN KEY (switch) REFERENCES Switch_Virtuel(id_switch)
);

CREATE TABLE IF NOT EXISTS Groupe(
    id_groupe INT NULL NULL AUTO_INCREMENT,
    nom VARCHAR(20),
    idPool INT,
    PRIMARY KEY (id_groupe),
    CONSTRAINT fk_groupe_poolInfra FOREIGN KEY (idPool) REFERENCES PoolInfra(id_pool)
);

CREATE TABLE IF NOT EXISTS Utilisateur(
    id_utilisateur INT NOT NULL AUTO_INCREMENT,
    nom VARCHAR(20),
    pseudo VARCHAR(20),
    mot_de_passe VARCHAR(20),
    idGroupe INT,
    PRIMARY KEY (id_utilisateur),
    CONSTRAINT fk_utilisateur_groupe FOREIGN KEY (idGroupe) REFERENCES Groupe(id_groupe)
);
