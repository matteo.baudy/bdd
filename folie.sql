DROP TABLE IF EXISTS Ticket;
DROP TABLE IF EXISTS Adherent;
DROP TABLE IF EXISTS Tombola;
DROP TABLE IF EXISTS Possede;

CREATE OR REPLACE TABLE Tombola(
    id_tombola  INT AUTO_INCREMENT,
    prix    INT NOT NULL,
    numero_gagnant  INT NOT NULL,
    PRIMARY KEY (id_tombola),
    CONSTRAINT ck_prix CHECK (prix > 0)
);

CREATE OR REPLACE TABLE Ticket(
    id_ticket INT AUTO_INCREMENT,
    ticket_numero  INT NOT NULL,
    id_ticket_tombola INT NOT NULL,
    PRIMARY KEY (id_ticket),
    CONSTRAINT fk_ticket_tombola FOREIGN KEY (id_ticket_tombola) REFERENCES Tombola(id_tombola)
);

CREATE OR REPLACE TABLE Adherent(
    id_adh  INT AUTO_INCREMENT,
    nom VARCHAR(20) NOT NULL,
    prenom  VARCHAR(20) NOT NULL,
    date_adh    DATE DEFAULT CURRENT_DATE,
    somme_gagnee    INT DEFAULT 0,    
    idTicket   INT NOT NULL,
    PRIMARY KEY (id_adh),
    CONSTRAINT fk_ticket_adherent FOREIGN KEY (idTicket) REFERENCES Ticket(id_ticket),
    CONSTRAINT ck_somme_gagnee CHECK (somme_gagnee >= 0)
);

CREATE OR REPLACE TABLE Possede(
    numero INT NOT NULL REFERENCES Ticket(ticket_numero),
    idTombola INT NOT NULL REFERENCES Tombola(id_tombola)
--     CONSTRAINT fk_possede_tombola FOREIGN KEY (idTombola) REFERENCES Tombola(id_tombola),
--     CONSTRAINT fk_possede_ticket FOREIGN KEY (numero) REFERENCES Ticket(ticket_numero)
);