DROP TABLE IF EXISTS Region;
DROP TABLE IF EXISTS Vaccin;
DROP TABLE IF EXISTS Protocole;
DROP TABLE IF EXISTS Vaccination;
DROP TABLE IF EXISTS Infection;
DROP TABLE IF EXISTS Variant;
DROP TABLE IF EXISTS Type_traitement;
DROP TABLE IF EXISTS Traitement;
DROP TABLE IF EXISTS Etablissement;
DROP TABLE IF EXISTS Personne;

CREATE TABLE IF NOT EXISTS Region(
    id_region INT NOT NULL AUTO_INCREMENT,
    nom VARCHAR(20),
    PRIMARY KEY (id_region)
);

CREATE TABLE IF NOT EXISTS Vaccin(
    id_vaccin INT NOT NULL AUTO_INCREMENT,
    nom VARCHAR(20),
    fabriquant VARCHAR(20),
    type_vaccin VARCHAR(20),
    PRIMARY KEY (id_vaccin)
);

CREATE TABLE IF NOT EXISTS Protocole(
    id_protocole INT NOT NULL AUTO_INCREMENT,
    nom VARCHAR(20),
    PRIMARY KEY (id_protocole)
);

CREATE TABLE IF NOT EXISTS Personne(
    id_personne INT NOT NULL AUTO_INCREMENT,
    sexe ENUM('HOMME', 'FEMME'),
    age INT,
    id_region INT,
    PRIMARY KEY (id_personne),
    CONSTRAINT fk_personne_region FOREIGN KEY (id_region) REFERENCES Region(id_region),
    CONSTRAINT ck_age CHECK (0 < age < 100)
);

CREATE TABLE IF NOT EXISTS Vaccination (
    id_vaccination INT NOT NULL AUTO_INCREMENT,
    date_vaccination DATE DEFAULT CURRENT_DATE,
    id_personne INT,
    id_vaccin INT,
    PRIMARY KEY (id_vaccination),
    CONSTRAINT fk_vaccination_personne FOREIGN KEY (id_personne) REFERENCES Personne(id_personne),
    CONSTRAINT fk_vaccination_vaccin FOREIGN KEY(id_vaccin) REFERENCES Vaccin(id_vaccin)
);


CREATE TABLE IF NOT EXISTS Variant (
    id_variant INT NOT NULL AUTO_INCREMENT,
    nom VARCHAR(20),
    date_apparition_variant DATE DEFAULT CURRENT_DATE,
    PRIMARY KEY (id_variant)
);

CREATE TABLE IF NOT EXISTS Infection(
    id_infection INT NOT NULL AUTO_INCREMENT,
    date_infection DATE DEFAULT CURRENT_DATE,
    id_variant INT,
    id_personne INT,
    PRIMARY KEY (id_infection),
    CONSTRAINT fk_infection_variant FOREIGN KEY(id_variant) REFERENCES Variant(id_variant),
    CONSTRAINT fk_infection_personne FOREIGN KEY(id_personne)REFERENCES Personne(id_personne)
);

CREATE TABLE IF NOT EXISTS Etablissement (
    id_etablissement INT NOT NULL AUTO_INCREMENT,
    nom VARCHAR(20),
    PRIMARY KEY (id_etablissement)
);
CREATE TABLE IF NOT EXISTS Type_traitement(
    id_type_traitement INT NOT NULL AUTO_INCREMENT,
    descriptif VARCHAR(20),
    PRIMARY KEY (id_type_traitement)
);

CREATE TABLE IF NOT EXISTS Traitement (
    id_traitement INT NOT NULL AUTO_INCREMENT,
    date_mise_en_place DATE DEFAULT CURRENT_DATE,
    idInfection INT,
    idType_traitement INT,
    idEtablissement_sante INT,
    PRIMARY KEY (id_traitement),
    CONSTRAINT fk_traitement_infection FOREIGN KEY(idInfection)REFERENCES Infection(id_infection),
    CONSTRAINT fk_traitement_typeTraitement FOREIGN KEY(idType_traitement)REFERENCES Type_traitement(id_type_traitement),
    CONSTRAINT fk_traitement_etablissement FOREIGN KEY(idEtablissement_sante)REFERENCES Etablissement(id_etablissement)
);